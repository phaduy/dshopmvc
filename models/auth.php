<?php
class Auth
{
  static function isValidLogin($userName, $password){
    $password = md5($password);

    $db = DB::getInstance();

    $req = $db->prepare('SELECT user.code 
                        FROM role_user 
                            INNER JOIN role ON role_user.role_code = role.code 
                            INNER JOIN user ON role_user.user_code = user.code 
                        WHERE user.code = :userName AND user.password = :password');
    $req->execute(array(
                    'userName' => $userName,
                    'password' => $password));
    
    return $req->rowCount() > 0 ? true : false;
  }
}