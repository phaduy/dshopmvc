<?php
class Category
{
    public $id;
    public $code;
    public $name;
    public $des;

    function __construct($id, $code, $name, $des)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->des = $des;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM category WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new Category($item['id'], $item['code'], $item['name'], $item['description']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM category WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new Category($item['id'], $item['code'], $item['name'], $item['description']);
    }
    return null;
  }

  static function getCategoryName($code)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM category WHERE code = :code AND delete_flg = 0');
    $req->execute(array('code' => $code));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return $item['name'];
    }
    return null;
  }


}