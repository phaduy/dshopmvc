<?php
class Role
{
    public $id;
    public $code;
    public $name;
    public $version;

    function __construct($id, $code, $name, $version)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->version = $version;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM role WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new Role($item['id'], $item['code'], $item['name'], $item['version']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM role WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new Role($item['id'], $item['code'], $item['name'], $item['version']);
    }
    return null;
  }

  static function add($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO role(code, name, create_user_code, create_date, delete_flg, version) 
                            VALUES (:code, :name, :user, :currDate, 0, 1)');

    return $req->execute(array(
                        'code' => $object->code,
                        'name' => $object->name,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d')
                        ));
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE role 
                        SET name = :name, update_user_code = :user, update_date = :currDate, version = :version 
                        WHERE id = :id');

    return $req->execute(array(
                        'id' => $object->id,
                        'name' => $object->name,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d'),
                        'version' => (int)$object->version + 1
                        ));
  }

  static function logicDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE role SET delete_flg = 1 WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM role WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}