<?php
class User
{
    public $id;
    public $code;
    public $password;
    public $name;
    public $address;
    public $dob;
    public $phone;
    public $email;
    public $version;

    function __construct($id, $code, $password, $name, $address, $dob, $phone, $email, $version)
    {
        $this->id = $id;
        $this->code = $code;
        $this->password = $password;
        $this->name = $name;
        $this->address = $address;
        $this->dob = $dob;
        $this->phone = $phone;
        $this->email = $email;
        $this->version = $version;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM user WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new User($item['id'], $item['code'], $item['password'], $item['name'], $item['address'], $item['dob'], $item['phone'], $item['email'], $item['version']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM user WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new User($item['id'], $item['code'], $item['password'], $item['name'], $item['address'], $item['dob'], $item['phone'], $item['email'], $item['version']);
    }
    return null;
  }

  static function isExistsByCode($code)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM user WHERE code = :code AND delete_flg = 0');
    $req->execute(array('code' => $code));

    if($req->rowCount() > 0){
        return true;
    }

    return false;
  }

  static function add($object)
  {
    $dob = empty($object->dob) ? null : $object->dob;
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO user(code, password, name, address, dob, phone, email, create_user_code, create_date, delete_flg, version) 
                            VALUES (:code, :password, :name, :address, :dob, :phone, :email, :user, :currDate, 0, 1)');

    return $req->execute(array(
                        'code' => $object->code,
                        'password' => $object->password,
                        'name' => $object->name,
                        'address' => $object->address,
                        'dob' => $dob,
                        'phone' => $object->phone,
                        'email' => $object->email,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d')
                        ));
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE user 
                        SET password = :password, name = :name, address = :address, 
                            dob = :dob, phone = :phone, email = :email,
                            update_user_code = :user, update_date = :currDate, version = :version 
                        WHERE id = :id');

    return $req->execute(array(
                        'id' => $object->id,
                        'password' => $object->password,
                        'name' => $object->name,
                        'address' => $object->address,
                        'dob' => $object->dob,
                        'phone' => $object->phone,
                        'email' => $object->email,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d'),
                        'version' => (int)$object->version + 1
                        ));
  }

  static function logicDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE user SET delete_flg = 1 WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM user WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}