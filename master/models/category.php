<?php
class Category
{
    public $id;
    public $code;
    public $name;
    public $des;
    public $version;

    function __construct($id, $code, $name, $des, $version)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->des = $des;
        $this->version = $version;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM category WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new Category($item['id'], $item['code'], $item['name'], $item['description'], $item['version']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM category WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new Category($item['id'], $item['code'], $item['name'], $item['description'], $item['version']);
    }
    return null;
  }

  static function isExistsByCode($code)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM category WHERE code = :code AND delete_flg = 0');
    $req->execute(array('code' => $code));

    if($req->rowCount() > 0){
        return true;
    }

    return false;
  }

  static function add($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO category(code, name, description, create_user_code, create_date, delete_flg, version) 
                            VALUES (:code, :name, :des, :user, :currDate, 0, 1)');

    return $req->execute(array(
                        'code' => $object->code,
                        'name' => $object->name,
                        'des' => $object->des,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d')
                        ));
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE category 
                        SET name = :name, description = :des, update_user_code = :user, update_date = :currDate, version = :version 
                        WHERE id = :id');

    return $req->execute(array(
                        'id' => $object->id,
                        'name' => $object->name,
                        'des' => $object->des,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d'),
                        'version' => (int)$object->version + 1
                        ));
  }

  static function logicDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE category SET delete_flg = 1 WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM category WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}