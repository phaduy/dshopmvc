<?php
class RoleUser
{
    public $id;
    public $roleCode;
    public $userCode;
    public $version;

    function __construct($id, $roleCode, $userCode, $version)
    {
        $this->id = $id;
        $this->roleCode = $roleCode;
        $this->userCode = $userCode;
        $this->version = $version;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM role_user WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new RoleUser($item['id'], $item['role_code'], $item['user_code'], $item['version']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM role_user WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new RoleUser($item['id'], $item['role_code'], $item['user_code'], $item['version']);
    }
    return null;
  }

  static function isExistsByCode($roleCode, $userCode)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM role_user WHERE role_code = :role_code AND user_code = :user_code AND delete_flg = 0');
    $req->execute(array(
                        'role_code' => $roleCode,
                        'user_code' => $userCode,
                        ));

    if($req->rowCount() > 0){
        return true;
    }

    return false;
  }

  static function add($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO role_user(role_code, user_code, create_user_code, create_date, delete_flg, version) 
                            VALUES (:role_code, :user_code, :user, :currDate, 0, 1)');

    return $req->execute(array(
                        'role_code' => $object->roleCode,
                        'user_code' => $object->userCode,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d')
                        ));
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE role_user 
                        SET role_code = :role_code, user_code = :user_code, update_user_code = :user, update_date = :currDate, version = :version 
                        WHERE id = :id');

    return $req->execute(array(
                        'id' => $object->id,
                        'role_code' => $object->roleCode,
                        'user_code' => $object->user_code,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d'),
                        'version' => (int)$object->version + 1
                        ));
  }

  static function logicDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE role_user SET delete_flg = 1 WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM role_user WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}