<?php
class ProductView
{
    public $id;
    public $productCode;
    public $views;

    function __construct($id, $productCode, $views)
    {
        $this->id = $id;
        $this->productCode = $productCode;
        $this->views = $views;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM product_view');

    foreach ($req->fetchAll() as $item) {
      $list[] = new ProductView($item['id'], $item['product_code'], $item['views']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM product_view WHERE id = :id');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new ProductView($item['id'], $item['product_code'], $item['views']);
    }
    return null;
  }

  static function add($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO product_view(product_code, views) 
                            VALUES (:productCode, :views)');

    return $req->execute(array(
                        'productCode' => $object->productCode,
                        'views' => $object->views
                        ));
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE product_view 
                        SET product_code = :productCode, views = :views 
                        WHERE id = :id');

    return $req->execute(array(
                        'productCode' => $object->productCode,
                        'views' => $object->views
                        ));
  }

  static function increaseViews($productCode)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE product_view SET views = views + 1 WHERE product_code = :productCode');
    
    return $req->execute(array('productCode' => $productCode));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM category WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}