<?php
class Product
{
    public $id;
    public $code;
    public $name;
    public $price;
    public $des;
    public $categoryCode;
    public $supplierCode;
    public $version;

    function __construct($id, $code, $name, $price, $des, $categoryCode, $supplierCode, $version)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->price = $price;
        $this->des = $des;
        $this->categoryCode = $categoryCode;
        $this->supplierCode = $supplierCode;
        $this->version = $version;
    }

  static function all(){
    $list = [];
    $db = DB::getInstance();
    $req = $db->query('SELECT * FROM product WHERE delete_flg = 0');

    foreach ($req->fetchAll() as $item) {
      $list[] = new Product($item['id'], $item['code'], $item['name'], $item['price'], $item['description'], $item['category_code'], $item['supplier_code'], $item['version']);
    }

    return $list;
  }

  static function find($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM product WHERE id = :id AND delete_flg = 0');
    $req->execute(array('id' => $id));

    $item = $req->fetch();
    if (isset($item['id'])) {
      return new Product($item['id'], $item['code'], $item['name'], $item['price'], $item['description'], $item['category_code'], $item['supplier_code'], $item['version']);
    }
    return null;
  }

  static function isExistsByCode($code)
  {
    $db = DB::getInstance();
    $req = $db->prepare('SELECT * FROM product WHERE code = :code AND delete_flg = 0');
    $req->execute(array('code' => $code));

    if($req->rowCount() > 0){
        return true;
    }

    return false;
  }

  static function add($object)
  {
    $dob = empty($object->dob) ? null : $object->dob;
    $db = DB::getInstance();
    $req = $db->prepare('INSERT INTO product(code, name, price, description, category_code, supplier_code, create_user_code, create_date, delete_flg, version) 
                            VALUES (:code, :name, :price, :description, :category_code, :supplier_code, :user, :currDate, 0, 1)');

    $req->execute(array(
                        'code' => $object->code,
                        'name' => $object->name,
                        'price' => $object->price,
                        'description' => $object->des,
                        'category_code' => $object->categoryCode,
                        'supplier_code' => $object->supplierCode,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d')
                        ));

   return $db->lastInsertId();
  }

  static function update($object)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE product 
                        SET name = :name, price = :price, description = :description, 
                            category_code = :category_code, supplier_code = :supplier_code,
                            update_user_code = :user, update_date = :currDate, version = :version 
                        WHERE id = :id');

    $req->execute(array(
                        'id' => $object->id,
                        'name' => $object->name,
                        'price' => $object->price,
                        'description' => $object->des,
                        'category_code' => $object->categoryCode,
                        'supplier_code' => $object->supplierCode,
                        'user' => $_SESSION['userName'],
                        'currDate' => date('Y/m/d'),
                        'version' => (int)$object->version + 1
                        ));
                        
    return $object->id;
  }

  static function logicDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('UPDATE product SET delete_flg = 1 WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

  static function physDel($id)
  {
    $db = DB::getInstance();
    $req = $db->prepare('DELETE FROM product WHERE id = :id');

    return $req->execute(array('id' => $id));
  }

}