<?php
require_once('controllers/base_controller.php');
require_once('controllers/helpers/file.php');
require_once('models/product.php');
require_once('models/product_view.php');
require_once('models/category.php');

class ProductsController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function index()
  {
    $list = Product::all();
    $data = array('products' => $list);

    $this->render('product_list', $data);
  }

  public function detail(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;

    $detail = Product::find($id);
    $category = Category::all();

    $data = array(
      'products' => $detail,
      'categories' => $category,
      'suppliers' => ['WMA01', 'BLUES']
    );

    $this->render('product_detail', $data);
  }

  public function update(){
    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $code = isset($_POST['code']) ? $_POST['code'] : null;
    $name = isset($_POST['name']) ? $_POST['name'] : null;
    $price = isset($_POST['price']) ? $_POST['price'] : null;
    $des = isset($_POST['des']) ? $_POST['des'] : null;
    $categoryCode = isset($_POST['categoryCode']) ? $_POST['categoryCode'] : null;
    $supplierCode = isset($_POST['supplierCode']) ? $_POST['supplierCode'] : null;
    $version = isset($_POST['version']) ? $_POST['version'] : null;
    $file = isset($_POST['file']) ? $_POST['file'] : null;    

    $errorMsg = array('errorMsg' => null);

    if(empty($code) || empty($name) || empty($price)){
      $errorMsg['errorMsg'] = "Code and name and price are required !";
      print_r(json_encode($errorMsg));
      exit();
    }

    if(empty($id)){
      if(!(bool)Product::isExistsByCode($code)){
        $id = Product::add(new Product(null, $code, $name, $price, $des, $categoryCode, $supplierCode, 1));
        if(!empty($id)){
          ProductView::add(new ProductView(null, $code, 0));
        }
      } else {
        $errorMsg['errorMsg'] = "Product code '".$code."' is exists !";
        print_r(json_encode($errorMsg));
        exit();
      }
    } else{
        $id = Product::update(new Product($id, $code, $name, $price, $des, $categoryCode, $supplierCode, $version));
    }

    if((bool)$id){
      $target_dir = "../assets/images/product/".$id."/";
      isset($_FILES['file']) ? File::upload($_FILES['file'], $target_dir) : null;
      $errorMsg['errorMsg'] = isset($_FILES['file']) ? File::upload($_FILES['file'], $target_dir) : null;
    } else {
      $errorMsg['errorMsg'] = "The processed data is not correct !";
    }

    print_r(json_encode($errorMsg));
  }

  public function logicDel(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    
    echo (bool)Product::logicDel($id);
  }

  public function error()
  {
    $this->render('error');
  }
}