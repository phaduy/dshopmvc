<?php
require_once('controllers/base_controller.php');
require_once('models/auth.php');

class AuthController extends BaseController
{
  function __construct()
  {
    $this->folder = 'layouts';
  }

  public function index()
  {
    $data = array();
    $this->loginRender('login', $data);
  }

  public function error()
  {
    $this->render('error');
  }

  public function login()
  {
    $data = array(
        'errorMsg' => null
    );

    $userName = isset($_POST['userName']) ? $_POST['userName'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;
    
    if(empty($userName) || empty($password)){
        $data['errorMsg'] = "User name and password is required !";
    }

    if(!Auth::isValidLogin($userName, $password)){
        session_unset();
        $data['errorMsg'] = "User name and password incorrect !";
    }

    if(empty($data['errorMsg'])){
        $_SESSION['userName'] = $userName;
        $_SESSION['password'] = md5($password);
    }
    
    print_r(json_encode($data));
  }

  public function logout(){
    session_unset();
    session_destroy();
  }
}