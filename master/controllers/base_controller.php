<?php
class BaseController
{
  protected $folder;
  
  function render($file, $data = array())
  {
    $view_file = 'views/' . $this->folder . '/' . $file . '.php';
    
    if(empty($_SESSION['userName'])){
      require_once('views/layouts/login.php');
      exit();
    }
    
    if (is_file($view_file)) {
      extract($data);

      ob_start();
      require_once($view_file);
      $content = ob_get_clean();
      
      require_once('views/layouts/application.php');
      
    } else {
      header('Location: index.php?controller=pages&action=error');
    }
  }

  function loginRender($file, $data = array())
  {
    require_once('views/layouts/login.php');
  }
}