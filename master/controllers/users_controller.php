<?php
require_once('controllers/base_controller.php');
require_once('models/user.php');
require_once('models/role.php');
require_once('models/role_user.php');

class UsersController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function index()
  {
    $list = User::all();
    $data = array('users' => $list);

    $this->render('user_list', $data);
  }

  public function detail(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;

    $detail = User::find($id);
    $role = Role::all();
    $data = array(
      'users' => $detail,
      'roles' => $role);

    $this->render('user_detail', $data);
  }

  public function update(){
    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $code = isset($_POST['code']) ? $_POST['code'] : null;
    $password = isset($_POST['password']) ? md5($_POST['password']) : null;
    $roleCode = isset($_POST['roleCode']) ? $_POST['roleCode'] : null;
    $name = isset($_POST['name']) ? $_POST['name'] : null;
    $address = isset($_POST['address']) ? $_POST['address'] : null;
    $dob = isset($_POST['dob']) ? $_POST['dob'] : null;
    $phone = isset($_POST['phone']) ? $_POST['phone'] : null;
    $email = isset($_POST['email']) ? $_POST['email'] : null;
    $version = isset($_POST['version']) ? $_POST['version'] : null;

    $errorMsg = array('errorMsg' => null);

    if(empty($code) || empty($password) || empty($name) || empty($roleCode)){
      $errorMsg['errorMsg'] = "Code and name and password are required !";
      print_r(json_encode($errorMsg));
      exit();
    }

    $isValid = false;
    if(empty($id)){
      if(!(bool)User::isExistsByCode($code)){
        $isValid = (bool)User::add(new User(null, $code, $password, $name, $address, $dob, $phone, $email, 1));
      } else{
        $errorMsg['errorMsg'] = "User code '".$code."' is exists !";
        print_r(json_encode($errorMsg));
        exit();
      }
    } else{
      $isValid = (bool)User::update(new User($id, $code, $password, $name, $address, $dob, $phone, $email, $version));
    }
    
    if(!(bool)RoleUser::isExistsByCode($roleCode, $code) && $isValid){
      RoleUser::add(new RoleUser(null, $roleCode, $code, 1));
    }

    if(!(bool)$isValid){
      $errorMsg['errorMsg'] = "The processed data is not correct !";
    }

    print_r(json_encode($errorMsg));
  }

  public function logicDel(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    
    echo (bool)User::logicDel($id);
  }

  public function error()
  {
    $this->render('error');
  }
}