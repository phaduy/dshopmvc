<?php
require_once('controllers/base_controller.php');
require_once('models/category.php');

class CategoriesController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function index()
  {
    $list = Category::all();
    $data = array('categories' => $list);

    $this->render('category_list', $data);
  }

  public function detail(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;

    $detail = Category::find($id);
    $data = array('categories' => $detail);

    $this->render('category_detail', $data);
  }

  public function update(){
    $id = isset($_POST['id']) ? $_POST['id'] : null;
    $code = isset($_POST['code']) ? $_POST['code'] : null;
    $name = isset($_POST['name']) ? $_POST['name'] : null;
    $des = isset($_POST['des']) ? $_POST['des'] : null;
    $version = isset($_POST['version']) ? $_POST['version'] : null;

    $errorMsg = array('errorMsg' => null);
    $isValid = false;

    if(empty($code) || empty($name)){
      $errorMsg['errorMsg'] = "Category code and category name are required !";
      print_r(json_encode($errorMsg));
      exit();
    }

    if(empty($id)){
      if((bool)Category::isExistsByCode($code)){
          $errorMsg['errorMsg'] = "Category code '".$code."' is exists !";
          print_r(json_encode($errorMsg));
          exit();
      } else{
        $isValid = (bool)Category::add(new Category(null, $code, $name, $des, 1));
      }
    } else{
      $isValid = (bool)Category::update(new Category($id, $code, $name, $des, $version));
    }

    if(!(bool)$isValid){
      $errorMsg['errorMsg'] = "The processed data is not correct !";
    }

    print_r(json_encode($errorMsg));
  }

  public function logicDel(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    
    echo (bool)Category::logicDel($id);
  }

  public function error()
  {
    $this->render('error');
  }
}