<?php

class File {
    static function upload($file, $target_dir){
        self::checkDirectory( $target_dir);
        $target_file = $target_dir . basename($file["name"]);
        $target_new_file_name = $target_dir . "thumb.jpg";
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check == false) {
                return "File is not an image.";
            }
        }

        // // Check if file already exists
        // if (file_exists($target_file)) {
        // echo "Sorry, file already exists.";
        // $uploadOk = 0;
        // }

        // Check file size
        if ($file["size"] > 2000000) {
            return "Sorry, your file is too large.";
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            return "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }

        // if everything is ok, try to upload file
        if (move_uploaded_file($file["tmp_name"], $target_new_file_name)) {
            // echo "The file ". htmlspecialchars( basename( $file["name"])). " has been uploaded.";
        } else {
            // return "Sorry, there was an error uploading your file.";
        }

        return null;
    }

    static function removeDirectory($path){
        if(is_dir($path)){
            if(!rmdir($path)) {
                echo ("Could not remove $path");
            }
        } else {
            echo ("File path $path not found !");
        }
        
    }

    static function checkDirectory($path){
        if(!is_dir($path)){
            mkdir($path);
        }
    }
}
?>