<body>
    <h1>
        <?php
            if(empty($users->id)){
                echo "New User";
            } else{
                echo "[".$users->code."]".$users->name;
            }
        ?>
    </h1>
    <hr>
    <div>
        <form action="javascript:;">
            <div id="errorMsg"></div>
            <div class="line-form">
                <span class="label">Code</span>
                <span class="info">
                    <?php
                        if(empty($users->id)){
                            ?><input type="text" name="code" id="code"><?php
                        } else{
                            ?><input type="hidden" name="code" id="code" value="<?php if(!empty($users->code)) echo $users->code; ?>"><?php
                            echo $users->code;
                        }
                     ?>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Name</span>
                <span class="info"><input type="text" name="name" id="name" value="<?php if(!empty($users->name)) echo $users->name; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Password</span>
                <span class="info"><input type="password" name="password" id="password" value="<?php if(!empty($users->password)) echo $users->password; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Role</span>
                <span class="info">
                    <select name="roleCode" id="roleCode">
                        <?php 
                            foreach($roles as $role){
                                ?>
                                    <option value="<?php echo $role->code; ?>"><?php echo $role->name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Address</span>
                <span class="info"><input type="text" name="address" id="address" value="<?php if(!empty($users->address)) echo $users->address; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">DOB</span>
                <span class="info"><input type="text" name="dob" id="dob" value="<?php if(!empty($users->dob)) echo $users->dob; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Phone</span>
                <span class="info"><input type="text" name="phone" id="phone" value="<?php if(!empty($users->phone)) echo $users->phone; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Email</span>
                <span class="info"><input type="text" name="email" id="email" value="<?php if(!empty($users->email)) echo $users->email; ?>"></span>
            </div>
            <div class="button-form">
                <input class="submit" type="submit" value="Save" id="btn_save">
                <input class="cancle" type="button" value="Cancle" onclick="window.location='?controller=users';">
            </div>

            <input type="hidden" name="id" id="id" value="<?php if(!empty($users->id)) echo $users->id; ?>">
            <input type="hidden" name="version" id="version" value="<?php if(!empty($users->version)) echo $users->version; ?>">
        </form>
    </div>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_detail.css">

<script>
    $(document).ready(function(){
        $('#btn_save').click(function(){
            var data = {
                id: $('#id').val(),
                code: $('#code').val(),
                name: $('#name').val(),
                password: $('#password').val(),
                roleCode: $('#roleCode').val(),
                address: $('#address').val(),
                dob: $('#dob').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
                version: $('#version').val()
            };

            $.post("?controller=users&action=update", data, function(res, status){
                var result = JSON.parse(res);
                if(result.errorMsg){
                    $('#errorMsg').html(result.errorMsg);
                } else{
                    window.location = '?controller=users';
                }
            });
        });
    });
</script>