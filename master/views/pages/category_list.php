<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<body>
    <h1>Master Category</h1>
    <div>
        <div class="btn-add">
            <input type="button" value="Add New" onclick="window.location='?controller=categories&action=detail'">
        </div>
        <!-- <div class="pagination">
            <a href="#">&laquo;</a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">&raquo;</a>
        </div> -->
        <table>
            <tr class="title">
                <th>Code</th>
                <th>Name</th>
                <th>Desciption</th>
            </tr>
            
            <?php
                foreach($categories as $category){
                    ?>
                        <tr>
                            <td><a href="?controller=categories&action=detail&id=<?php echo $category->id; ?>"><?php echo $category->code; ?></a></td>
                            <td><a href="?controller=categories&action=detail&id=<?php echo $category->id; ?>"><?php echo $category->name; ?></a></td>
                            <td><a href="?controller=categories&action=detail&id=<?php echo $category->id; ?>"><?php echo $category->des; ?></a></td>
                            <td class="del-btn"><a href="javascript:;" onclick="logicDel(<?php echo $category->id.',\''.$category->code.'\',\''.$category->name.'\''; ?>);">Delete</a></td>
                        </tr>
                    <?php
                }
            ?>
        </table>
    </div>

    <script>
        function logicDel(id, code, name){
            if(confirm('Are you sure you want to delete category ['+code+']'+name+'?')){
                $.get("?controller=categories&action=logicDel&id="+id, function(data, status){
                    if(data != null){
                        window.location="?controller=categories";
                    }
                });
            }
        }
    </script>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_list.css">
