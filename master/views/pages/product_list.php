<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<body>
    <h1>Master Product</h1>
    <div>
        <div class="btn-add">
            <input type="button" value="Add New" onclick="window.location='?controller=products&action=detail'">
        </div>
        <!-- <div class="pagination">
            <a href="#">&laquo;</a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">&raquo;</a>
        </div> -->
        <table>
            <tr class="title">
                <th>Image</th>
                <th>Code</th>
                <th>Name</th>
                <th>Price</th>
                <th>Category Code</th>
                <th>Supplier Code</th>
                <th>Desciption</th>
            </tr>
            
            <?php
                foreach($products as $product){
                    ?>
                        <tr>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>">
                                <img src="../assets/images/product/<?php echo $product->id.'/thumb.jpg'; ?>" width="100px" alt="">
                            </a></td>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>"><?php echo $product->code; ?></a></td>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>"><?php echo $product->name; ?></a></td>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>"><?php echo $product->price; ?></a></td>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>"><?php echo $product->categoryCode; ?></a></td>
                            <td><a href="?controller=products&action=detail&id=<?php echo $product->id; ?>"><?php echo $product->supplierCode; ?></a></td>
                            <td class="del-btn"><a href="javascript:;" onclick="logicDel(<?php echo $product->id.',\''.$product->code.'\',\''.$product->name.'\''; ?>);">Delete</a></td>
                        </tr>
                    <?php
                }
            ?>
        </table>
    </div>

    <script>
        function logicDel(id, code, name){
            if(confirm('Are you sure you want to delete product ['+code+']'+name+'?')){
                $.get("?controller=products&action=logicDel&id="+id, function(data, status){
                    if(data != null){
                        window.location="?controller=products";
                    }
                });
            }
        }
    </script>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_list.css">
