<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<body>
    <h1>Master Category</h1>
    <div>
        <div class="btn-add">
            <input type="button" value="Add New" onclick="window.location='?controller=users&action=detail'">
        </div>
        <!-- <div class="pagination">
            <a href="#">&laquo;</a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">&raquo;</a>
        </div> -->
        <table>
            <tr class="title">
                <th>Code</th>
                <th>Name</th>
                <th>Address</th>
                <th>DOB</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>
            
            <?php
                foreach($users as $user){
                    ?>
                        <tr>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->code; ?></a></td>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->name; ?></a></td>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->address; ?></a></td>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->dob; ?></a></td>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->phone; ?></a></td>
                            <td><a href="?controller=users&action=detail&id=<?php echo $user->id; ?>"><?php echo $user->email; ?></a></td>
                            <td class="del-btn"><a href="javascript:;" onclick="logicDel(<?php echo $user->id.',\''.$user->code.'\',\''.$user->name.'\''; ?>);">Delete</a></td>
                        </tr>
                    <?php
                }
            ?>
        </table>
    </div>

    <script>
        function logicDel(id, code, name){
            if(confirm('Are you sure you want to delete user ['+code+']'+name+'?')){
                $.get("?controller=users&action=logicDel&id="+id, function(data, status){
                    if(data != null){
                        window.location="?controller=users";
                    }
                });
            }
        }
    </script>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_list.css">
