<body>
    <h1>
        <?php
            if(empty($categories->id)){
                echo "New Category";
            } else{
                echo "[".$categories->code."]".$categories->name;
            }
        ?>
    </h1>
    <hr>
    <div>
        <form action="javascript:;">
            <div id="errorMsg"></div>
            <div class="line-form">
                <span class="label">Code</span>
                <span class="info">
                    <?php
                        if(empty($categories->id)){
                            ?><input type="text" name="code" id="code"><?php
                        } else{
                            ?><input type="hidden" name="code" id="code" value="<?php if(!empty($categories->code)) echo $categories->code; ?>"><?php
                            echo $categories->code;
                        }
                     ?>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Name</span>
                <span class="info"><input type="text" name="name" id="name" value="<?php if(!empty($categories->name)) echo $categories->name; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Description</span>
                <span class="info"><input type="text" name="des" id="des" value="<?php if(!empty($categories->des)) echo $categories->des; ?>"></span>
            </div>
            <div class="button-form">
                <input class="submit" type="submit" value="Save" id="btn_save">
                <input class="cancle" type="button" value="Cancle" onclick="window.location='?controller=categories';">
            </div>

            <input type="hidden" name="id" value="<?php if(!empty($categories->id)) echo $categories->id; ?>" id="id">
            <input type="hidden" name="version" value="<?php if(!empty($categories->version)) echo $categories->version; ?>" id="version">
        </form>
    </div>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_detail.css">

<script>
    $(document).ready(function(){
        $('#btn_save').click(function(){
            var data = {
                id: $('#id').val(),
                code: $('#code').val(),
                name: $('#name').val(),
                des: $('#des').val(),
                version: $('#version').val()
            };

            $.post("?controller=categories&action=update", data, function(res, status){
                var result = JSON.parse(res);
                if(result.errorMsg){
                    $('#errorMsg').html(result.errorMsg);
                } else{
                    window.location = '?controller=categories';
                }
            });
        });
    });
</script>