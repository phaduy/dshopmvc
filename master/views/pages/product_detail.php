<body>
    <h1>
        <?php
            if(empty($products->id)){
                echo "New Product";
            } else{
                echo "[".$products->code."]".$products->name;
            }
        ?>
    </h1>
    <hr>
    <div>
        <form action="javascript:;" method="post" enctype="multipart/form-data">
            <div id="errorMsg"></div>
            <div class="line-form">
                <span class="label">Image</span>
                <span class="info"><input type="file" name="file" id="file"></span>
            </div>
            <div class="line-form">
                <span class="label">Code</span>
                <span class="info">
                    <?php
                        if(empty($products->id)){
                            ?><input type="text" name="code" id="code"><?php
                        } else{
                            ?><input type="hidden" name="code" id="code" value="<?php if(!empty($products->code)) echo $products->code; ?>"><?php
                            echo $products->code;
                        }
                     ?>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Name</span>
                <span class="info"><input type="text" name="name" id="name" value="<?php if(!empty($products->name)) echo $products->name; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Price</span>
                <span class="info"><input type="text" name="price" id="price" value="<?php if(!empty($products->price)) echo $products->price; ?>"></span>
            </div>
            <div class="line-form">
                <span class="label">Category</span>
                <span class="info">
                    <select name="categoryCode" id="categoryCode">
                        <?php
                            foreach($categories as $category){
                                ?><option value="<?php echo $category->code; ?>"><?php echo '['.$category->code.'] '.$category->name; ?></option><?php
                            }
                        ?>
                    </select>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Supplier Code</span>
                <span class="info">
                <select name="supplierCode" id="supplierCode">
                        <?php
                            foreach($suppliers as $supplier){
                                ?><option value="<?php echo $supplier; ?>"><?php echo '['.$supplier.'] '.$supplier; ?></option><?php
                            }
                        ?>
                    </select>
                </span>
            </div>
            <div class="line-form">
                <span class="label">Description</span>
                <span class="info"><input type="text" name="des" id="des" value="<?php if(!empty($products->des)) echo $products->des; ?>"></span>
            </div>
            <div class="button-form">
                <input class="submit" type="submit" value="Save" id="btn_save">
                <input class="cancle" type="button" value="Cancle" onclick="window.location='?controller=products';">
            </div>

            <input type="hidden" name="id" id="id" value="<?php if(!empty($products->id)) echo $products->id; ?>">
            <input type="hidden" name="version" id="version" value="<?php if(!empty($products->version)) echo $products->version; ?>">
        </form>
    </div>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/master_detail.css">

<script>
    $(document).ready(function(){
        $('#btn_save').click(function(){
            var file_data = $('#file').prop('files')[0] ? $('#file').prop('files')[0] : null;
            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('id', $('#id').val());
            form_data.append('code', $('#code').val());
            form_data.append('name', $('#name').val());
            form_data.append('price', $('#price').val());
            form_data.append('des', $('#des').val());
            form_data.append('categoryCode', $('#categoryCode').val());
            form_data.append('supplierCode', $('#supplierCode').val());
            form_data.append('version', $('#version').val());

            $.ajax({
                url: '?controller=products&action=update', // gửi đến file upload.php 
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (res) {
                    var result = JSON.parse(res);
                    if(result.errorMsg){
                        $('#errorMsg').html(result.errorMsg);
                    } else{
                        window.location = '?controller=products';
                    }
                }
            });
        });
    });
</script>