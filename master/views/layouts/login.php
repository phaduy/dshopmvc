<DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DShop MVC</title>
    <link rel="stylesheet" type="text/css" href="./assets/css/login-form.css">
  </head>
  <body>
      <div class="container">
      <form action="javascript:;">
            <div class="line-form">
                <span class="label">User name:</span class="info"><input type="text" name="userName" id="userName">
            </div>
            <div class="line-form">
                <span class="label">Password:</span class="info"><input type="password" name="password" id="password">
            </div>
            <div class="right-input">
                <span class="label"></span>
                <span><input type="checkbox">Remember Me</span>
                <span><a href="javscript:;">Forgot Password?</a></span>
            </div>
            <div class="right-input">
                <span class="label"></span>
                <span id="errorMsg" style="color: red;"></span>
            </div>
            <div class="right-input">
                <span class="label"></span>
                <input class="btn-login" type="submit" value="Login" id="btn_login">
            </div>
        </form>
      </div>
  </body>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</html>

<script>
    $(document).ready(function(){
        $('#btn_login').click(function(){
            var userName = $('#userName').val();
            var password = $('#password').val();

            if(userName && password){
                $.post("?controller=auth&action=login", {userName: userName, password: password}, function(res, status){
                    var data = JSON.parse(res);
                    if(data.errorMsg){
                        $('#errorMsg').html(data.errorMsg);
                    }else{
                        window.location = '';
                    }
                });
            }
            else{
                $('#errorMsg').html('User name and password is required !');
            }
        });
    });
</script>