<DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DShop MVC</title>
    <link rel="stylesheet" type="text/css" href="./assets/css/application.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="header">
      <?php include("login_info.php"); ?>
    </div>
    <div class="clearfix">
    <ul>
          <li><a href="index.php?controller=categories">Category</a></li>
          <li><a href="index.php?controller=products">Product</a></li>
          <li><a href="index.php?controller=users">User</a></li>
        </ul>
      <div class="content">
        <?= @$content ?>
      </div>
    </div>
  </body>
</html>