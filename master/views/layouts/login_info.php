<?php
    if(isset($_SESSION['userName']) && isset($_SESSION['password'])){
        ?>
        <form action="">
            <span class="login-bar">
                Welcome <?php echo $_SESSION['userName'] ?>
                <input type="submit" value="Logout" id="btn_logout">
            </span>     
        </form>
        <?php
    }
?>

<style>
    .login-bar{
        float: right;
        padding-right: 10px;
    }

    .login-bar input{
        color: white;
        background: #4CAF50;
    }
</style>

<script>
    $(document).ready(function(){
        $('#btn_logout').click(function(){
            $.post("?controller=auth&action=logout", function(res, status){});
        });
    });
</script>