<?php
require_once('controllers/base_controller.php');
require_once('models/product.php');
require_once('models/category.php');

class PagesController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function home()
  {
    $productNew = Product::productNew();
    $productMostView = Product::productMostView();
    
    $categoryProductList = array();
    $categories = Category::all();
    foreach($categories as $category){
      $productByCategoryCode = Product::productCategory($category->code, 8);
      $arr = array($category->name.'/'.$category->code => $productByCategoryCode);
      $categoryProductList = array_merge($categoryProductList, $arr);
    }

    $data = array(
      'productNews' => $productNew,
      'productMostViews' => $productMostView,
      'categoryProductList' => $categoryProductList
    );

    $this->render('home', $data);
  }

  public function error()
  {
    $this->render('error');
  }
}