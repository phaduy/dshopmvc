<?php
require_once('controllers/base_controller.php');
require_once('models/category.php');

class CategoriesController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function index(){}

  public function allCategoryAjax()
  {
    echo json_encode(Category::all());
  }

  public function error()
  {
    $this->render('error');
  }
}