<?php
require_once('controllers/base_controller.php');
require_once('models/product.php');
require_once('models/category.php');
require_once('master/models/product_view.php');

class ProductsController extends BaseController
{
  function __construct()
  {
    $this->folder = 'pages';
  }

  public function index()
  {
    $all = Product::all();
    $productNew = Product::productNew();
    $productMostView = Product::productMostView();
    $data = array(
      'products' => $all,
      'productNews' => $productNew,
      'productMostViews' => $productMostView
    );

    $this->render('home', $data);
  }

  public function all()
  {
    $list = Product::all();
    $data = array('products' => $list);

    $this->render('product', $data);
  }

  public function productCategory()
  {
    $categoryCode = isset($_GET['categoryCode']) ? $_GET['categoryCode'] : null;

    $list = Product::productCategory($categoryCode);
    $categoryName = Category::getCategoryName($categoryCode);
    $data = array(
      'categoryName' => $categoryName,
      'products' => $list);

    $this->render('product', $data);
  }

  public function detail(){
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    $code = isset($_GET['code']) ? $_GET['code'] : null;

    ProductView::increaseViews($code);
    $detail = Product::find($id);
    $data = array('products' => $detail);

    $this->render('detail', $data);
  }

  public function error()
  {
    $this->render('error');
  }
}