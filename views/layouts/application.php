<DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DShop MVC</title>
    <link rel="stylesheet" type="text/css" href="./assets/css/application.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
  <div class="head-info"></div>
        <div class="head-image">
            <img src="./assets/images/img-head.jpg" alt="">
        </div>
        <div>
            <div class="navigation-bar">
                <ul>
                    <li><a href="index.php" class="active">Home</a></li>
                    <li><a href="?controller=products&action=all">Product</a></li>
                    <li><a href="javascript:;">News</a></li>
                    <li><a href="javascript:;">About</a></li>
                    <li><a href="javascript:;">Contact</a></li>
                    <?php include("login.php"); ?>
                </ul>
            </div>
        </div>
        <div class="clearfix">
            <?php include("category.php"); ?>
            <div class="content">
              <?= @$content ?>
            </div>
            <div class="side-right"></div>
        </div>
        <div class="footer"></div>
  </body>

  <script>
    $(document).ready(function(){
        $('.navigation-bar li a').click(function(){
            $('.navigation-bar li a').removeClass('active');
            $(this).addClass('active');
        })
    })
</script>
</html>