<?php
    session_start();
    if(isset($_SESSION['userName']) && isset($_SESSION['password'])){
        ?>
        <form action="">
            <li class="login-bar" style="float: right">
                Welcome <?php echo $_SESSION['userName'] ?> !
                <input type="submit" id="btn_logout" value="Logout">
            </li>     
        </form>
        <?php
    }
    else{
        ?>
        <form action="" id="frmLogin">
            <li class="login-bar" style="float: right">
                User name: <input type="text" size="10" name="userName" id="userName">
                Password: <input type="password" size="10" name="password" id="password">
                <input type="submit" id="btn_login" value="Login">
                <span class="forget-pass-link"><a href="javascript:;">Forget password</a></span>
                <div><?php if(!empty($errorMsg)) echo $errorMsg; ?></div>
            </li>
        </form>            
        <?php
    }
?>

<script>
    $(document).ready(function(){
        $('#btn_login').click(function(){
            var userName = $('#userName').val();
            var password = $('#password').val();

            if(userName && password){
                $.post("?controller=auth&action=login", {userName: userName, password: password}, function(res, status){
                    var data = JSON.parse(res);
                    if(data.errorMsg){
                        alert(data.errorMsg);
                    }
                });
            }
            else{
                alert('User name and password is required !');
            }
        });


        $('#btn_logout').click(function(){
            $.post("?controller=auth&action=logout", function(res, status){});
        });

    });
</script>