<div class="side-left">
    <span id="category_result"></span>
</div>

  <script>
    $(document).ready(function(){
        var html = '';
        $.get("?controller=categories&action=allCategoryAjax", function(data, status){
            var data = JSON.parse(data);
            html += "<ul>";
            for(var category of data){
                html += "<li><a href='index.php?controller=products&action=productCategory&categoryCode=" +category.code+"'>"+category.name+"</a></li>";
            }
            html += "</ul>";

            $('#category_result').html(html);
        });
    })
</script>