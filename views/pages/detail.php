<body>
    <div>
        <img class="img-detail" src="assets/images/product/<?php echo $products->id; ?>/thumb.jpg" onerror="this.onerror=null; this.src='assets/images/default.jpg'">

        <div class="container">
            <div class="product-name"><?php echo $products->name; ?></div>
            <div class="product-code">Product Code: <?php echo $products->code; ?></div>
            <div class="cart-info">
                <span class="price"><?php echo number_format($products->price); ?>$</span>
                <span class="label-quantity">Quantity:</span>
                <span class="input-quantity"><input type="text"></span>
                <span class="btn-add-to-cart"><input type="button" value="Add to cart"></span>
            </div>
            <div>
                <span class="label">Availability: </span>
                <span class="info">In Stock</span>
            </div>
            <div>
                <span class="label">Condition: </span>
                <span class="info">New</span>
            </div>
            <div>
                <span class="label">Brand: </span>
                <span class="info">D-SHOP</span>
            </div>
            <div>
                <span class="label">Views: </span>
                <span class="info"><?php echo $products->views; ?></span>
            </div>
        </div>
    </div>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/detail.css">