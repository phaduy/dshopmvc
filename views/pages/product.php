<body>
    <h1 class="title">
        <?php
            if(isset($_GET['action']) && $_GET['action'] == 'productCategory'){
                echo $categoryName.' Category';
            } else{
                echo 'All Product';
            }
        ?>
    </h1>
    <?php
        if(empty($products)){
            ?><div class="msg-empty">Product is not found !</div><?php
        } else {
    ?>
    <div class="product-list">
        <?php
            foreach($products as $product){
                ?>
                <div class="box">
                    <div>
                        <a href="index.php?controller=products&action=detail&id=<?php echo $product->id; ?>&code=<?php echo $product->code; ?>">
                            <img src="assets/images/product/<?php echo $product->id; ?>/thumb.jpg" width="180px" onerror="this.onerror=null; this.src='assets/images/default.jpg'">
                        </a>
                    </div>
                    <div class="product-name"><?php echo $product->name; ?></div>
                    <div class="product-price"><?php echo number_format($product->price); ?>$</div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/product.css">