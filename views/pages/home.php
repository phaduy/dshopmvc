<body>
  <div class="product-container">
    <h1 class="title">New Products</h1>
    <div class="product-list">
      <?php
        foreach($productNews as $product){
          ?>
            <div class="box">
              <a href="index.php?controller=products&action=detail&id=<?php echo $product->id; ?>&code=<?php echo $product->code; ?>">
                <img src="assets/images/product/<?php echo $product->id; ?>/thumb.jpg" width="180px" onerror="this.onerror=null; this.src='assets/images/default.jpg'">
              </a>
              <div class="product-name"><?php echo $product->name; ?></div>
              <div class="product-price"><?php echo number_format($product->price); ?>$</div>
          </div>
          <?php
        }
      ?>
    </div>
  </div>

  <div class="product-container">
    <h1 class="title">View Product</h1>
    <div class="product-list">
    <?php
        foreach($productMostViews as $product){
          ?>
            <div class="box">
              <a href="index.php?controller=products&action=detail&id=<?php echo $product->id; ?>&code=<?php echo $product->code; ?>">
                <img src="assets/images/product/<?php echo $product->id; ?>/thumb.jpg" width="180px" onerror="this.onerror=null; this.src='assets/images/default.jpg'">
              </a>
              <div class="product-name"><?php echo $product->name; ?></div>
              <div class="product-price"><?php echo number_format($product->price); ?>$</div>
          </div>
          <?php
        }
      ?>
    </div>
  </div>
  <!-- <div>
    <h1 class="title">Category 1</h1>
  </div>
  <div>
    <h1 class="title">Category 2</h1>
  </div>
  <div>
    <h1 class="title">Category 3</h1>
  </div> -->
  <?php
    foreach($categoryProductList as $categoryCode => $productList){
      if(empty($productList)) continue;
      $categoryName = strstr($categoryCode, '/', true);
      ?>
        <div class="product-container">
          <h1 class="title"><?php echo $categoryName; ?> Category</h1>
          <div class="product-list">
          <?php
              foreach($productList as $product){
                ?>
                  <div class="box">
                    <a href="index.php?controller=products&action=detail&id=<?php echo $product->id; ?>&code=<?php echo $product->code; ?>">
                      <img src="assets/images/product/<?php echo $product->id; ?>/thumb.jpg" width="180px" onerror="this.onerror=null; this.src='assets/images/default.jpg'">
                    </a>
                    <div class="product-name"><?php echo $product->name; ?></div>
                    <div class="product-price"><?php echo number_format($product->price); ?>$</div>
                </div>
                <?php
              }
            ?>
          </div>
        </div>
      <?php
    }
  ?>
</body>

<link rel="stylesheet" type="text/css" href="assets/css/product.css">